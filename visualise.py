import glob, sys, os
sys.path.insert(0, os.path.abspath('volume_manipulation'))
from volume_manipulation.visualisations import visualise_3d_grid, show_grid
from volume_manipulation.volume_manip import load_numpy_array


if __name__=='__main__':
    volume_names = list()
    if not os.path.exists('volumes'):
        print("No volume file exists.")
    else:
        os.chdir('volumes')
        num_of_files = 0
        for file in glob.glob("*.npy"):
            volume_names.append(file)
            num_of_files+=1
        #print the array names to the user 
        for i in range(num_of_files):
            print(f"{i}: {volume_names[i]}", end='')
            if i !=num_of_files-1:print(', ', end='')
        # print()
        chosen_number = input('\nEnter the number corresponding to the chosen volume:')
        if not chosen_number.strip().isdigit():
            print("User input is not a number.")
        else:
            chosen_number = int(chosen_number)
            #get the number of the array chosen by the user
            if chosen_number in range(num_of_files):
                volume_name = volume_names[chosen_number]
                print('Visualising volume '+volume_name+'.')
            else:
                print('Selected number corresponds to no volume.')
                exit()
            # load the chosen array and visualise it with PyVista
            volume = load_numpy_array(volume_name)
            visualise_3d_grid(volume)
            # show_grid(volume)
