# 3D carotid artery reconstruction from 2D in-vitro ultrasound images

This is a README for the the bachelor project [3D carotid artery reconstruction from 2D in-vitro ultrasound images](https://gitlab.fel.cvut.cz/kvasoma1/bakalarska_prace).

The goal of the project is to create a 3D model of the carotid artery from 2D ultrasound slices. 
## Data
A few datasets are present in this project, located in folder data. More can be downloaded from [carotid artery images](https://drive.google.com/drive/folders/1Vh4dlS3hPiq-GXgxg1VDJ5Vw3fit7xhR?usp=sharing) if needed.

## Required libraries
The project is implemented in Python 3.
The required libraries are numpy, scipy, SimpleITK, pyvista, Pillow, numba, opencv. They can be downloaded with pip by the command:
`pip3 install -r requirements.txt`
(There might be some problems with the numba installation in that case [](https://stackoverflow.com/a/65850900))

## Running the program
run the script `run.py`, which creates the 3D volume, saves it in a folder named volumes and opens a window with the created 3D visualisation.
### Required script parameters:
1. name of the folder which includes the transverse and longitudinal images(these need to be separated into two folders named transverse, and longitudinal).
(*The folder containing the images needs to be located in the data folder*)

Running the script looks like:
`python3 run.py --image_dir="name_of_dir""`

### Example
An example command of running the script can look like:
`python3 run.py --image_dir="100027"`

### Result of running the script
Running the script results in the 3D volume being saved under the name `vol_name_of_dir.npy` in a directory called volumes, if this directory does not exist, it is created automatically.
A window displaying the resulting volume is then opened.

## The final 3D visualisation
3D visualisation windown opens after running the main script `run.py`(described above), or after running the script `visualise.py`.

The script `visualise.py` takes no parameters. It is run by the command 
`python3 visualise.py`
The script searches this directory for the directory volumes, where the volumes should be saved.
Then it asks the user to choose an array to view by pressing a corresponding number (for example 0, 1, 2...).

On the left side of the opened window, there is a 3D contour of the volume. The values of the volume that are currently shown can be controlled with a slider, which is controlled by the mouse cursor.

On the right side, there are three orthogonal slices of the created volume. The desired slice to be shown can be selected by dragging the slice to the selected place.

Zooming in and out can be done by the mousewheel in both visualisations separately.
Rotating is done by 'grabbing' and moving the mouse anywhere outside the volume in the left visualisation and by 'grabbing' and moving outside the red borders in the right visualisation.
